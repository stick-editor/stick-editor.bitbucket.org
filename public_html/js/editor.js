/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global fabric */


const Tools = {
    SELECT: 'select',
    WIRE: 'wire',
    DELETE: 'delete',
    LINK: 'link',
    UNLINK: 'unlink'
};
Object.freeze(Tools);

const gridSize = 20;
Object.freeze(gridSize);

const Layers = {
    POLY: {
        INDEX: 0,
        NAME: 'Poly',
        COLOR: '#ff0000',
        WIDTH: gridSize,
        LINECAP: 'square'
    },
    METAL1: {
        INDEX: 1,
        NAME: 'Metal-1',
        COLOR: '#0000ff',
        WIDTH: 6,
        LINECAP: 'round'
    },
    METAL2: {
        INDEX: 5,
        NAME: 'Metal-2',
        COLOR: '#E50FE5',
        WIDTH: 6,
        LINECAP: 'round'
    },
    POWER: {
        INDEX: -1,
        NAME: 'Vdd',
        COLOR: '#0000ff',
        WIDTH: gridSize,
        LINECAP: 'round'
    },
    GROUND: {
        INDEX: -1,
        NAME: 'Ground',
        COLOR: '#0000ff',
        WIDTH: gridSize,
        LINECAP: 'round'
    },
    PACTIVE: {
        INDEX: -3,
        NAME: 'P',
        COLOR: '#ED7D31',
        WIDTH: gridSize,
        LINECAP: 'round'
    },
    NACTIVE: {
        INDEX: -3,
        NAME: 'N',
        COLOR: '#70AD47',
        WIDTH: gridSize,
        LINECAP: 'round'
    },
    CONTACT: {
        INDEX: 3,
        NAME: 'Contact',
        COLOR: 'black',
        WIDTH: gridSize - 8,
        LINECAP: 'round'
    },
    VIA: {
        INDEX: 7,
        NAME: 'Contact',
        COLOR: 'darkgray',
        WIDTH: gridSize - 8,
        LINECAP: 'round'
    },
    GRAY: {
        INDEX: 99,
        NAME: 'Gray',
        COLOR: 'gray',
        WIDTH: 6,
        LINECAP: 'butt'
    }
};
Object.freeze(Layers);

function compare(a, b) {
    return (a.zIndex - b.zIndex);
}

var currentTool = Tools.SELECT;
var currentLayer = Layers.POLY;

const canvas = new fabric.Canvas('stick');
canvas.renderOnAddRemove = false;
canvas.defaultCursor = 'url(img/pointer.png), auto';
canvas.hoverCursor = 'url(img/link.png) 16 16,auto';
canvas.selectionColor = '';
canvas.selectionBorderColor = 'black';
canvas.selectionLineWidth = 1;
canvas.selectionDashArray = [3, 5];
canvas.preserveObjectStacking = true;
var drawing = false;
var path = [];
var pathCircle = null;
var poly = null;
var pathLines = [];
const canvasRect = document.getElementById('stick').getBoundingClientRect();
const numCols = canvasRect.width / gridSize;
const numLins = canvasRect.height / gridSize;

var polyLines = new Array(numCols).fill(null);
var contacts = new Array(numCols).fill(0).map(() => new Array(numLins).fill(null));
var vias = new Array(numCols).fill(0).map(() => new Array(numLins).fill(null));
var hMetal1Lines = new Array(numCols).fill(0).map(() => new Array(numLins).fill(null));
var vMetal1Lines = new Array(numCols).fill(0).map(() => new Array(numLins).fill(null));
var hMetal2Lines = new Array(numCols).fill(0).map(() => new Array(numLins).fill(null));
var vMetal2Lines = new Array(numCols).fill(0).map(() => new Array(numLins).fill(null));

function snapToGrid(point)
{
    var rect = document.getElementById('stick').getBoundingClientRect();
    return ({x: point.x - ((point.x - rect.left) % gridSize) - rect.left,
        y: point.y - ((point.y - rect.top) % gridSize) - rect.top});
}

function gridPoint(point)
{
    var grid = {col: Math.trunc((point.x) / gridSize),
        lin: Math.trunc((point.y) / gridSize)};
    console.assert((grid.col >= 0 && grid.lin >= 0),
            "Error computing grid point for (" + point.x + "," + point.y + ")" +
            "grid (" + grid.col + "," + grid.lin + ")");
    return grid;
}

function mouseToGridPoint(point)
{
    var rect = document.getElementById('stick').getBoundingClientRect();
    var grid = {col: Math.trunc((point.x - rect.left) / gridSize),
        lin: Math.trunc((point.y - rect.top) / gridSize)};
    console.assert((grid.col >= 0 && grid.lin >= 0),
            "Error computing grid point for (" + point.x + "," + point.y + ")" +
            "rect left=" + rect.left + " top=" + rect.top +
            "grid (" + grid.col + "," + grid.lin + ")");
    return grid;
}

function canvasPoint(grid, center) {
    var point = {x: grid.col * gridSize, y: grid.lin * gridSize};
    if (center) {
        (point.x) += gridSize / 2;
        (point.y) += gridSize / 2;
    }
    return (point);
}

/**
 * Draw the grid on the canvas.
 * @param {fabric.Canvas} canvas
 * @param {int} width
 * @param {int} height
 * @param {int} gridSize
 * @returns {fabric.Canvas} 
 */
function drawGrid(canvas, width, height, gridSize)
{
    //lines
    var grid = [];
    for (i = 0; i < height; i += gridSize) {
        grid.push(new fabric.Line([0, i, width, i], {
            stroke: 'lightgray',
            strokeWidth: 1,
            selectable: false,
            evented: false,
            strokeDashArray: [2, 1],
            zIndex: -99
        }));
        //canvas.add(grid);
    }
    //cols
    for (i = 0; i < width; i += gridSize) {
        grid.push(new fabric.Line([i, 0, i, height], {
            stroke: 'lightgray',
            strokeWidth: 1,
            selectable: false,
            evented: false,
            strokeDashArray: [2, 1],
            zIndex: -99
        }));
        //canvas.add(grid);
    }
    canvas.add(new fabric.Group(grid, {
        left: 0,
        top: 0,
        selectable: false,
        evented: false,
        zIndex: -99
    }));
    return canvas;
}

/**
 * Draw the VDD line at top grid.
 * @param {fabric.Canvas} canvas
 * @param {type} width
 * @param {type} height
 * @param {type} gridSize
 * @returns {fabric.Canvas}
 */
function drawVdd(canvas, width, height, gridSize)
{
    // "add" rectangle onto canvas
    canvas.add(new fabric.Rect({
        left: 0,
        top: gridSize,
        fill: Layers.POWER.COLOR,
        width: width,
        height: Layers.POWER.WIDTH,
        selectable: false,
        evented: false,
        zIndex: Layers.POWER.INDEX
    }));
    //add text
    canvas.add(new fabric.Text(Layers.POWER.NAME, {
        fontSize: 16, top: (gridSize + 2),
        left: (width - 40),
        fill: 'white',
        selectable: false,
        evented: false,
        zIndex: 100
    }));
    return canvas;
}

function drawGround(canvas, width, height, gridSize)
{
    canvas.add(new fabric.Rect({
        left: 0,
        top: height - 2 * gridSize,
        fill: '#0000ff',
        width: width,
        height: Layers.GROUND.WIDTH,
        selectable: false,
        evented: false,
        zIndex: Layers.GROUND.INDEX
    }));
    canvas.add(new fabric.Text(Layers.GROUND.NAME, {
        fontSize: 16,
        top: height - 2 * gridSize,
        left: width - 75,
        fill: 'white',
        selectable: false,
        evented: false,
        zIndex: 100
    }));
}

function drawPActive(canvas, canvasWidth, gridSize)
{
    canvas.add(new fabric.Rect({
        left: 0,
        top: 6 * gridSize,
        fill: Layers.PACTIVE.COLOR,
        width: canvasWidth,
        height: Layers.PACTIVE.WIDTH,
        opacity: 0.6,
        selectable: false,
        evented: false,
        zIndex: Layers.PACTIVE.INDEX
    }));
    canvas.add(new fabric.Text(Layers.PACTIVE.NAME, {
        fontSize: 16,
        top: 6 * gridSize,
        left: canvasWidth - 20,
        fill: 'white',
        selectable: false,
        evented: false,
        zIndex: 100
    }));
}

function drawNActive(canvas, canvasWidth, canvasHeight, gridSize)
{
    // "add" rectangle onto canvas
    canvas.add(new fabric.Rect({
        left: 0,
        top: canvasHeight - 7 * gridSize,
        fill: Layers.NACTIVE.COLOR,
        width: canvasWidth,
        height: Layers.NACTIVE.WIDTH,
        opacity: 0.6,
        selectable: false,
        evented: false,
        zIndex: Layers.NACTIVE.INDEX
    }));
    // legend
    canvas.add(new fabric.Text("N", {
        fontSize: 16,
        top: (canvasHeight - 7 * gridSize),
        left: (canvasWidth - 20),
        fill: 'white',
        selectable: false,
        evented: false,
        zIndex: 100
    }));
}

function createPoly(canvasHeight, x, gridSize, isAdding) {
    if (typeof createPoly.counter == 'undefined' ) {
        createPoly.counter = 1;
    }
    if (isAdding) {
        var poly = [
            new fabric.Rect({
            left: 0,
            top: 0,
            width: Layers.POLY.WIDTH,
            height: canvasHeight - 8 * gridSize,
            fill: Layers.POLY.COLOR,
            selectable: false,
            evented: true,
            zIndex: Layers.POLY.INDEX
            }),
            new fabric.Text(('I'+createPoly.counter), {
            fontSize: 14, 
            top: 2,
            left: 2,
            fill: 'white',
            selectable: false,
            evented: false,
            zIndex: 100
            })
];

        (createPoly.counter)++;
        
        return new fabric.Group(poly, {
            left: x,
            top: 4 * gridSize,
            selectable: false,
            evented: false,
            zIndex: Layers.POLY.INDEX
        });
    }
    return new fabric.Rect({
        left: x,
        top: 4 * gridSize,
        width: Layers.POLY.WIDTH,
        height: canvasHeight - 8 * gridSize,
        fill: Layers.POLY.COLOR,
        selectable: false,
        evented: true,
        zIndex: Layers.POLY.INDEX
    });
}

function createDot(point) {
    return new fabric.Circle({
        radius: 6,
        fill: 'gray',
        left: (point.x) - 6,
        top: (point.y) - 6,
        selectable: false,
        evented: false
    });
}

function createLine(from, to, layer, select, log) {

    if ((from.x != to.x) && (from.y != to.y)) {
        console.error("Only horizontal or vertical lines allowed!");
        return null;
    }

    // all lines will be left -> rigth or up -> down
    if (to.x < from.x || to.y < from.y)
        [from, to] = [to, from];

    var delta = layer.WIDTH / 2;
    var gridFrom = gridPoint(from);
    var gridTo = gridPoint(to);
    if (log)
        console.log("Line " + layer.NAME + "(" + gridFrom.col + "," + gridFrom.lin + ")-->(" + gridTo.col + "," + gridTo.lin + ")");

    var line = new fabric.Line([from.x - delta, from.y - delta, to.x - delta, to.y - delta], {
        strokeWidth: layer.WIDTH,
        stroke: layer.COLOR,
        strokeLineCap: layer.LINECAP,
        selectable: select,
        evented: select,
        zIndex: layer.INDEX,
        grid: {
            from: gridFrom,
            to: gridTo
        }
    });
    return line;
}

function createContact(point, gridSize) {
    var contact = [
        new fabric.Rect({
            left: 0,
            top: 0,
            width: gridSize - 4,
            height: gridSize - 4,
            fill: Layers.METAL1.COLOR,
            selectable: false,
            evented: true,
            zIndex: Layers.METAL1.INDEX
        }),
        new fabric.Rect({
            left: 2,
            top: 2,
            width: gridSize - 8,
            height: gridSize - 8,
            fill: Layers.CONTACT.COLOR,
            selectable: false,
            evented: true,
            zIndex: Layers.CONTACT.INDEX
        })
    ];

    return new fabric.Group(contact, {
        left: (point.x) + 2,
        top: (point.y) + 2,
        selectable: false,
        evented: false,
        zIndex: Layers.CONTACT.INDEX
    });
}

function createVia(point, gridSize) {
    var via = [
        new fabric.Rect({
            left: 0,
            top: 0,
            width: gridSize - 4,
            height: gridSize - 4,
            fill: Layers.METAL2.COLOR,
            selectable: false,
            evented: true,
            zIndex: Layers.METAL2.INDEX
        }),
        new fabric.Rect({
            left: 2,
            top: 2,
            width: gridSize - 8,
            height: gridSize - 8,
            fill: Layers.VIA.COLOR,
            selectable: false,
            evented: true,
            zIndex: Layers.VIA.INDEX
        })
    ];

    return new fabric.Group(via, {
        left: (point.x) + 2,
        top: (point.y) + 2,
        selectable: false,
        evented: false,
        zIndex: Layers.VIA.INDEX
    });
}

function addAndMinimizeLines(layer, newLines)
{
    var hLines;
    var vLines;
    switch (layer) {
        case Layers.METAL1:
            hLines = hMetal1Lines;
            vLines = vMetal1Lines;
            break;
        case Layers.METAL2:
            hLines = hMetal2Lines;
            vLines = vMetal2Lines;
            break;
        default:
            console.error("Cannot minimize layer" + layer.NAME);
            return;
            break;
    }
    newLines.forEach(function (line) {
        var from = line.grid.from;
        var to = line.grid.to;
        var updateGrid = true;
        var isVertical = false;

        if (from.col == to.col)
            isVertical = true;

        var start = null;
        var end = null;
        if (isVertical) {
            start = vLines[from.col][from.lin];
            end = vLines[to.col][to.lin];
        } else {
            start = hLines[from.col][from.lin];
            end = hLines[to.col][to.lin];
        }

        if (start == null && end == null)
        {
            // no lines to be joined
            // add this new one and delete any line in the middle
            var newLine = line;
            var current = Object.assign({}, from);
        } else if (start != null && end == null)
        {  // join to a left line
            // and delete all include midones
            from = start.grid.from;
            lineFrom = canvasPoint(from, true);
            //to = to; 
            lineTo = canvasPoint(to, true);
            var newLine = createLine(lineFrom, lineTo, layer, false);
            canvas.remove(line);
            canvas.add(newLine);
        } else if (start == null && end != null)
        {

            // join to a right line
            // and delete all include midones
            //from = from;
            lineFrom = canvasPoint(from, true);
            to = end.grid.to;
            lineTo = canvasPoint(to, true);
            var newLine = createLine(lineFrom, lineTo, layer, false);
            canvas.remove(line);
            canvas.add(newLine);
        } else if (start == end) {
            canvas.remove(line);
            updateGrid = false;
        } else
        {
            // join to lines in both sides
            // and delete all include midones
            from = start.grid.from;
            lineFrom = canvasPoint(from, true);
            to = end.grid.to;
            lineTo = canvasPoint(to, true);
            var newLine = createLine(lineFrom, lineTo, layer, false);
            canvas.remove(line);
            canvas.add(newLine);
        }
        // add new line to grid and
        // delete unsed lines contained into new one
        if (updateGrid) {
            var current = Object.assign({}, from);
            while (current.col <= to.col && current.lin <= to.lin) {
                if (isVertical) {
                    canvas.remove(vLines[current.col][current.lin]);
                    vLines[current.col][current.lin] = newLine;
                } else {
                    canvas.remove(hLines[current.col][current.lin]);
                    hLines[current.col][current.lin] = newLine;
                }
                if (isVertical)
                    (current.lin)++;
                else
                    (current.col)++;
            }
        }
    });
}
function removeLine(layer, coord)
{
    var hLines;
    var vLines;
    switch (layer) {
        case Layers.METAL1:
            hLines = hMetal1Lines;
            vLines = vMetal1Lines;
            break;
        case Layers.METAL2:
            hLines = hMetal2Lines;
            vLines = vMetal2Lines;
            break;
        default:
            console.error("Cannot delete lines on layer" + layer.NAME);
            return;
            break;
    }
    x = coord.col;
    y = coord.lin;
    //remove vertical line
    line = vLines[x][y];
    if (line != null) {
        do {
            y--;
        } while (y >= 0 && vLines[x][y] == line);
        y++;
        do {
            vLines[x][y] == null;
            y++;
        } while (y < numLins && vLines[x][y] == line);
        canvas.remove(line);
    }
    x = coord.col;
    y = coord.lin;
    //remove horizontal lines
    line = hLines[x][y];
    if (line != null) {
        do {
            x--;
        } while (x >= 0 && hLines[x][y] == line);
        x++;
        do {
            hLines[x][y] == null;
            x++;
        } while (x < numCols && hLines[x][y] == line);
        canvas.remove(line);
    }
}

function endWiring() {
    if (path.length > 0) {
        canvas.remove(pathCircle);
        canvas.remove(pathLines.pop());
        canvas.remove(pathLines.pop());
        addAndMinimizeLines(currentLayer, pathLines);
        path = [];
        pathLines = [];
        pathCircle = null;
    }
}

document.getElementById('layer').addEventListener('change', function (e) {
    var layerSel = e.currentTarget;
    var index = e.currentTarget.selectedIndex;
    var prevLayer = currentLayer;
    switch (layerSel.options[index].value) {
        case 'poly':
            currentLayer = Layers.POLY;
            break;
        case 'm1':
            currentLayer = Layers.METAL1;
            break;
        case 'm2':
            currentLayer = Layers.METAL2;
            break;
        default:
            console.error('Unknow layer selection');
            currentLayer = Layers.GRAY;
    }
    layerSel.style.color = currentLayer.COLOR;
    if (prevLayer != currentLayer &&
            (prevLayer == Layers.METAL1 || prevLayer == Layers.METAL2)) {
        endWiring();
    }
});

document.getElementById('export').addEventListener('click', function (e) {
});

document.getElementById('verify').addEventListener('click', function (e) {
});

document.getElementById('select').addEventListener('click', function (e) {
    endWiring();
    canvas.defaultCursor = 'url(img/pointer.png), auto';
    canvas.hoverCursor = 'move';
    canvas.selection = true;
    canvas.selectable = true;
    currentTool = Tools.SELECT;
});

document.getElementById('wire').addEventListener('click', function (e) {
    canvas.defaultCursor = 'url(img/pencil.png), auto';
    canvas.hoverCursor = 'url(img/pencil.png),auto';
    canvas.selection = false;
    currentTool = Tools.WIRE;
});
document.getElementById('delete').addEventListener('click', function (e) {
    endWiring();
    canvas.defaultCursor = 'url(img/delete.png) 14 16, auto';
    canvas.hoverCursor = 'url(img/delete.png) 14 16,auto';
    canvas.selection = false;
    canvas.selectable = false;
    currentTool = Tools.DELETE;
});


document.getElementById('link').addEventListener('click', function (e) {
    endWiring();
    canvas.defaultCursor = 'url(img/link.png) 16 16, auto';
    canvas.hoverCursor = 'url(img/link.png) 16 16,auto';
    canvas.selection = false;
    canvas.selectable = false;
    currentTool = Tools.LINK;
});

document.getElementById('unlink').addEventListener('click', function (e) {
    endWiring();
    canvas.defaultCursor = 'url(img/unlink.png) 16 16, auto';
    canvas.hoverCursor = 'url(img/unlink.png) 16 16,auto';
    canvas.selection = false;
    currentTool = Tools.UNLINK;
});


drawGrid(canvas, canvasRect.width, canvasRect.height, gridSize);
drawVdd(canvas, canvasRect.width, canvasRect.height, gridSize);
drawGround(canvas, canvasRect.width, canvasRect.height, gridSize);
drawPActive(canvas, canvasRect.width, gridSize);
drawNActive(canvas, canvasRect.width, canvasRect.height, gridSize);
canvas.requestRenderAll();

window.addEventListener('keypress', function (options) {
});

window.addEventListener('keydown', function (e) {
// almost there
// should add two lines (blue and gray to current mouse position
//  deleting is ok... put is messing up the vector (must add two lines)
//    if ((e.key === 'Escape'||e.key==='Esc'||e.keyCode===27) && (e.target.nodeName === 'BODY')) {
//        if (currentTool == Tools.WIRE &&
//          (currentLayer == Layers.METAL1 || currentLayer == Layers.METAL2) &&
//          path.length > 0 ) {
//            canvas.remove(pathCircle);
//            lineGray = pathLines.pop();
//            grid = lineGray.grid.to;
//            console.log('Line gray at '+grid.col+','+grid.lin);
//            canvas.remove(lineGray);
//            canvas.remove(pathLines.pop());
//            path.pop();
//            if (path.length > 0) {
//                pathCircle = createDot(path[path.length-1]);
//                canvas.add(pathCircle);
//            } else {
//                path = [];
//                pathLines = [];
//                pathCircle = null;
//            }
//            canvas._objects.sort(compare);
//            canvas.requestRenderAll();
//        }
//        e.preventDefault();
//        return false;
//    }
});

canvas.on('mouse:up', function (options) {
    var point = snapToGrid({x: options.e.clientX, y: options.e.clientY});
    var grid = mouseToGridPoint({x: options.e.clientX, y: options.e.clientY});
    switch (currentTool) {
        case Tools.WIRE:
            if (currentLayer === Layers.METAL1 || currentLayer === Layers.METAL2) {
                // adding metal layers
                //compute grid coords
                (point.x) += (gridSize / 2);
                (point.y) += (gridSize / 2);
                var actual = Object.assign({}, point);
                if (path.length === 0) {
                    pathCircle = createDot(point);
                    canvas.add(pathCircle);
                    path.push(point);
                } else {
                    var prev = path[path.length - 1];
                    canvas.remove(pathCircle);
                    canvas.remove(pathLines.pop());
                    canvas.remove(pathLines.pop());
                    if (prev.x === point.x && prev.y === point.y) {
                        // double click... end line
                        addAndMinimizeLines(currentLayer, pathLines);
                        path = [];
                        pathLines = [];
                        pathCircle = null;
                    } else {
                        if (Math.abs(prev.x - point.x) > Math.abs(prev.y - point.y)) {
                            point.y = prev.y;
                        } else {
                            point.x = prev.x;
                        }
                        pathCircle = createDot(point);

                        var lineBlue = createLine(prev, point, currentLayer, false, false);
                        var lineBlue2 = createLine(point, actual, currentLayer, false);
                        var lineGray = createLine(actual, actual, Layers.GRAY, false);
                        canvas.add(pathCircle);
                        canvas.add(lineBlue);
                        canvas.add(lineBlue2);
                        canvas.add(lineGray);
                        pathLines.push(lineBlue);
                        pathLines.push(lineBlue2);
                        pathLines.push(lineGray);
                        path.push(point);
                    }
                }
            } else {
                // adding POLY
                if (grid.col > 0 && grid.col < numCols - 1) {
                    if (polyLines[grid.col] == null && polyLines[grid.col - 1] == null && polyLines[grid.col + 1] == null) {
                        polyLines[grid.col] = createPoly(canvasRect.height, point.x, gridSize, true);
                        canvas.add(polyLines[grid.col]);
                    }
                }
            } // end currentLayer
            break;
        case Tools.DELETE:
            if (currentLayer === Layers.POLY) {
                canvas.remove(polyLines[grid.col]);
                polyLines[grid.col] = null;
            } else if (currentLayer === Layers.METAL1 || currentLayer === Layers.METAL2) {
                removeLine(currentLayer, grid);
            }
            break;
        case Tools.LINK:
            var index = document.getElementById("contact").selectedIndex;
            if (index == 1) {
                // via
                if (vias[grid.col][grid.lin] == null) {
                    vias[grid.col][grid.lin] = createVia(point, gridSize);
                    canvas.add(vias[grid.col][grid.lin]);
                }
            } else {
                //contact
                if (contacts[grid.col][grid.lin] == null) {
                    contacts[grid.col][grid.lin] = createContact(point, gridSize);
                    canvas.add(contacts[grid.col][grid.lin]);
                }
            }
            break;
        case Tools.UNLINK:
            var index = document.getElementById("contact").selectedIndex;
            if (index == 1) {
                // via
                canvas.remove(vias[grid.col][grid.lin]);
                vias[grid.col][grid.lin] = null;
            } else {
                //contact
                canvas.remove(contacts[grid.col][grid.lin]);
                contacts[grid.col][grid.lin] = null;
            }
            break;
        default:
            break;
    } // end switch tool
    canvas._objects.sort(compare);
    canvas.requestRenderAll();
});

canvas.on('mouse:move', function (options) {
    if (currentTool === Tools.WIRE) {
        var point = snapToGrid({x: options.e.clientX, y: options.e.clientY});
        if (currentLayer === Layers.POLY) {
            canvas.remove(poly);
            poly = createPoly(canvasRect.height, point.x, gridSize);
            poly.opacity = 0.5;
            canvas.add(poly);
        } else if ((currentLayer === Layers.METAL1 || currentLayer === Layers.METAL2) &&
                path.length > 0) {
            var prev = path[path.length - 1];
            (point.x) += (gridSize / 2);
            (point.y) += (gridSize / 2);
            var actual = Object.assign({}, point);
            if (Math.abs(prev.x - point.x) > Math.abs(prev.y - point.y)) {
                point.y = prev.y;
            } else {
                point.x = prev.x;
            }
            var lineBlue = createLine(prev, point, currentLayer, false);
            var lineGray = createLine(point, actual, Layers.GRAY, false);
            canvas.remove(pathLines.pop());
            canvas.remove(pathLines.pop());
            canvas.add(lineBlue);
            canvas.add(lineGray);
            pathLines.push(lineBlue);
            pathLines.push(lineGray);
        }
    }
    canvas._objects.sort(compare);
    canvas.requestRenderAll();
});

canvas.on('mouse:out', function (options) {
    if (poly != null) {
        canvas.remove(poly);
        canvas.requestRenderAll();
    }

});
