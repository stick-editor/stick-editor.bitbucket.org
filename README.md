# Stick Diagram Editor #

This is a simple stick diagram editor for transistor-level layout. This
intends to help in Integrated Circuit courses and it should be embedded into
Moodle environments and web pages.

### How do I get set up? ###

* Just download it and open "public_html/index.html" file in your browser
* You cant [try it online](https://stick-editor.bitbucket.io/public_html)

### Contribution guidelines ###

* Development
* Code review
* Testing
* Ideas

### Who do I talk to? ###

* [João Leonardo Fragoso](mailto:jl.fragoso@gmail.com?Subject=[StickEditor]%20Contact)